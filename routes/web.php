<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', ['as' => 'home.index', 'uses' => 'HomeController@index']);
Route::get('/login', 'LoginController@showLoginForm')->name('home.login');
Route::post('/submitlogin', 'LoginController@login')->name('home.login.submit');
Route::post('/logout', 'LoginController@logout')->name('home.logout');

Route::group(['middleware' => 'web'], function () {
	//HOMEPAGE
	Route::get('/dashboard', 'DashboardController@index')->name('home.dashboard');

	//PROFILE
	Route::get('profile/edit', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::post('profile/{id}/update', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);

	//JABATAN
    Route::get('jabatan', ['as' => 'jabatan.dashboard', 'uses' => 'JabatanController@index']);
    Route::get('jabatan/create', ['as' => 'jabatan.create', 'uses' => 'JabatanController@create']);
    Route::post('jabatan/submit', ['as' => 'jabatan.submit', 'uses' => 'JabatanController@submit']);
	Route::get('jabatan/{id}/edit', ['as' => 'jabatan.edit', 'uses' => 'JabatanController@edit']);
	Route::post('jabatan/{id}/update', ['as' => 'jabatan.update', 'uses' => 'JabatanController@update']);
	Route::get('jabatan/delete/{id}', ['as' => 'jabatan.delete', 'uses' => 'JabatanController@delete']);

	//DIVISI
    Route::get('divisi', ['as' => 'divisi.dashboard', 'uses' => 'DivisiController@index']);
    Route::get('divisi/create', ['as' => 'divisi.create', 'uses' => 'DivisiController@create']);
    Route::post('divisi/submit', ['as' => 'divisi.submit', 'uses' => 'DivisiController@submit']);
	Route::get('divisi/{id}/edit', ['as' => 'divisi.edit', 'uses' => 'DivisiController@edit']);
	Route::post('divisi/{id}/update', ['as' => 'divisi.update', 'uses' => 'DivisiController@update']);
	Route::get('divisi/delete/{id}', ['as' => 'divisi.delete', 'uses' => 'DivisiController@delete']);

	//JENIS CUTI
    Route::get('jenis-cuti', ['as' => 'jeniscuti.dashboard', 'uses' => 'JenisCutiController@index']);
    Route::get('jenis-cuti/create', ['as' => 'jeniscuti.create', 'uses' => 'JenisCutiController@create']);
    Route::post('jenis-cuti/submit', ['as' => 'jeniscuti.submit', 'uses' => 'JenisCutiController@submit']);
	Route::get('jenis-cuti/{id}/edit', ['as' => 'jeniscuti.edit', 'uses' => 'JenisCutiController@edit']);
	Route::post('jenis-cuti/{id}/update', ['as' => 'jeniscuti.update', 'uses' => 'JenisCutiController@update']);
	Route::get('jenis-cuti/delete/{id}', ['as' => 'jeniscuti.delete', 'uses' => 'JenisCutiController@delete']);

	//JENIS IZIN
    Route::get('jenis-izin', ['as' => 'jenisizin.dashboard', 'uses' => 'JenisIzinController@index']);
    Route::get('jenis-izin/create', ['as' => 'jenisizin.create', 'uses' => 'JenisIzinController@create']);
    Route::post('jenis-izin/submit', ['as' => 'jenisizin.submit', 'uses' => 'JenisIzinController@submit']);
	Route::get('jenis-izin/{id}/edit', ['as' => 'jenisizin.edit', 'uses' => 'JenisIzinController@edit']);
	Route::post('jenis-izin/{id}/update', ['as' => 'jenisizin.update', 'uses' => 'JenisIzinController@update']);
	Route::get('jenis-izin/delete/{id}', ['as' => 'jenisizin.delete', 'uses' => 'JenisIzinController@delete']);

	//KARYAWAN
    Route::get('karyawan', ['as' => 'karyawan.dashboard', 'uses' => 'KaryawanController@index']);
    Route::get('karyawan/create', ['as' => 'karyawan.create', 'uses' => 'KaryawanController@create']);
    Route::post('karyawan/submit', ['as' => 'karyawan.submit', 'uses' => 'KaryawanController@submit']);
	Route::get('karyawan/{id}/edit', ['as' => 'karyawan.edit', 'uses' => 'KaryawanController@edit']);
	Route::post('karyawan/{id}/update', ['as' => 'karyawan.update', 'uses' => 'KaryawanController@update']);
	Route::get('karyawan/delete/{id}', ['as' => 'karyawan.delete', 'uses' => 'KaryawanController@delete']);

	//PENGUMUMAN
    Route::get('pengumuman', ['as' => 'pengumuman.dashboard', 'uses' => 'PengumumanController@index']);
    Route::get('pengumuman/create', ['as' => 'pengumuman.create', 'uses' => 'PengumumanController@create']);
    Route::post('pengumuman/submit', ['as' => 'pengumuman.submit', 'uses' => 'PengumumanController@submit']);
	Route::get('pengumuman/{id}/edit', ['as' => 'pengumuman.edit', 'uses' => 'PengumumanController@edit']);
	Route::post('pengumuman/{id}/update', ['as' => 'pengumuman.update', 'uses' => 'PengumumanController@update']);
	Route::get('pengumuman/delete/{id}', ['as' => 'pengumuman.delete', 'uses' => 'PengumumanController@delete']);

	//ABSENSI
    Route::get('absensi', ['as' => 'absensi.dashboard', 'uses' => 'AbsensiController@index']);
    Route::get('absensi/create', ['as' => 'absensi.create', 'uses' => 'AbsensiController@create']);
    Route::post('absensi/submit', ['as' => 'absensi.submit', 'uses' => 'AbsensiController@submit']);
	Route::get('absensi/{id}/edit', ['as' => 'absensi.edit', 'uses' => 'AbsensiController@edit']);
	Route::post('absensi/{id}/update', ['as' => 'absensi.update', 'uses' => 'AbsensiController@update']);
	Route::get('absensi/delete/{id}', ['as' => 'absensi.delete', 'uses' => 'AbsensiController@delete']);

	//CUTI
    Route::get('cuti', ['as' => 'cuti.dashboard', 'uses' => 'CutiController@index']);
    Route::get('cuti/create', ['as' => 'cuti.create', 'uses' => 'CutiController@create']);
    Route::post('cuti/submit', ['as' => 'cuti.submit', 'uses' => 'CutiController@submit']);
	Route::get('cuti/{id}/edit', ['as' => 'cuti.edit', 'uses' => 'CutiController@edit']);
	Route::post('cuti/{id}/update', ['as' => 'cuti.update', 'uses' => 'CutiController@update']);
	Route::get('cuti/delete/{id}', ['as' => 'cuti.delete', 'uses' => 'CutiController@delete']);
	Route::get('cuti/terima/{id}', ['as' => 'cuti.terima', 'uses' => 'CutiController@terima']);
	Route::get('cuti/tolak/{id}', ['as' => 'cuti.tolak', 'uses' => 'CutiController@tolak']);

	//IZIN
    Route::get('izin', ['as' => 'izin.dashboard', 'uses' => 'IzinController@index']);
    Route::get('izin/create', ['as' => 'izin.create', 'uses' => 'IzinController@create']);
    Route::post('izin/submit', ['as' => 'izin.submit', 'uses' => 'IzinController@submit']);
	Route::get('izin/{id}/edit', ['as' => 'izin.edit', 'uses' => 'IzinController@edit']);
	Route::post('izin/{id}/update', ['as' => 'izin.update', 'uses' => 'IzinController@update']);
	Route::get('izin/delete/{id}', ['as' => 'izin.delete', 'uses' => 'IzinController@delete']);
	Route::get('izin/terima/{id}', ['as' => 'izin.terima', 'uses' => 'IzinController@terima']);
	Route::get('izin/tolak/{id}', ['as' => 'izin.tolak', 'uses' => 'IzinController@tolak']);

	//SAKIT
    Route::get('sakit', ['as' => 'sakit.dashboard', 'uses' => 'SakitController@index']);
    Route::get('sakit/create', ['as' => 'sakit.create', 'uses' => 'SakitController@create']);
    Route::post('sakit/submit', ['as' => 'sakit.submit', 'uses' => 'SakitController@submit']);
	Route::get('sakit/{id}/edit', ['as' => 'sakit.edit', 'uses' => 'SakitController@edit']);
	Route::post('sakit/{id}/update', ['as' => 'sakit.update', 'uses' => 'SakitController@update']);
	Route::get('sakit/delete/{id}', ['as' => 'sakit.delete', 'uses' => 'SakitController@delete']);
	Route::get('sakit/terima/{id}', ['as' => 'sakit.terima', 'uses' => 'SakitController@terima']);
	Route::get('sakit/tolak/{id}', ['as' => 'sakit.tolak', 'uses' => 'SakitController@tolak']);
});	    
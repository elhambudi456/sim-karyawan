<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfilTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profil')->insert([
            'user_id' => 1,
            'jabatan_id' => 1,
            'divisi_id' => 1,
            'image' => 'default.png',
            'phone' => '08123123123',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('profil')->insert([
            'user_id' => 2,
            'jabatan_id' => 1,
            'divisi_id' => 2,
            'image' => 'default.png',
            'phone' => '08123123123',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DivisiTableSeeder::class);
        $this->call(JabatanTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(ProfilTableSeeder::class);
        $this->call(JenisCutiTableSeeder::class);
        $this->call(JenisIzinTableSeeder::class);
    }
}
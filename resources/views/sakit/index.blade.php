@extends('layouts')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Data Sakit</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Sakit</a></li>
                        <li class="breadcrumb-item active">Beranda</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-12 col-12">
                    <div class="card">
                        <div class="card-header bg-maroon">
                            <h3 class="card-title">Data Sakit</h3>
                            <div class="card-tools">
                                <a href="{{ route('sakit.create') }}" class="btn-tool"
                                    style="color:white !important;"><i class="fa fa-plus"></i> Tambah</a>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <strong>{{ $message }}</strong>
                            </div>
                            @endif
                            @if ($message = Session::get('danger'))
                            <div class="alert alert-danger">
                                <strong>{{ $message }}</strong>
                            </div>
                            @endif
                            <table id="example1" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal Awal</th>
                                        <th>Tanggal Akhir</th>
                                        <th>Nama</th>
                                        <th>File</th>
                                        <th>Keterangan</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $key => $dataItem)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{ date('d F Y',strtotime($dataItem->tanggal_awal)) }}</td>
                                        <td>{{ date('d F Y',strtotime($dataItem->tanggal_akhir)) }}</td>
                                        <td>{{ $dataItem->karyawan($dataItem->user_id) }}</td>
                                        <td><a href="{{ asset('pengajuan-sakit/'.$dataItem->file) }}"
                                                download>{{ $dataItem->file }}</a>
                                        </td>
                                        <td>{{ !empty($dataItem->keterangan) ? $dataItem->keterangan : '-'  }}</td>
                                        <td>
                                            @if($dataItem->status == 'MENUNGGU PERSETUJUAN')
                                            <span class="badge badge-warning">MENUNGGU PERSETUJUAN</span>
                                            @elseif($dataItem->status == 'DITERIMA')
                                            <span class="badge badge-success">DITERIMA</span>
                                            @elseif($dataItem->status == 'DITOLAK')
                                            <span class="badge badge-danger">DITOLAK</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if(auth()->guard('web')->user()->role_id == 1)
                                            @if($dataItem->status == 'MENUNGGU PERSETUJUAN')
                                            <a href="{{ route('sakit.terima', $dataItem->id) }}" class="btn btn-success"
                                                onclick="return confirm('Apakah Anda Yakin Ingin Menerima Pengajuan Ini ??');"><span
                                                    class="fa fa-check"></span> Terima</a>
                                            <a href="{{ route('sakit.tolak', $dataItem->id) }}" class="btn btn-default"
                                                onclick="return confirm('Apakah Anda Yakin Ingin Menolak Pengajuan Ini ??');"><span
                                                    class="fa fa-times"></span> Tolak</a>
                                            @endif
                                            <a href="{{ route('sakit.edit', ['id'=>$dataItem->id]) }}"
                                                class="btn btn-warning"><span class="fa fa-edit"></span>
                                                Ubah</a>
                                            <a href="{{ route('sakit.delete', $dataItem->id) }}" class="btn btn-danger"
                                                onclick="return confirm('Apakah Anda Yakin Ingin Menghapus Ini ??');"><span
                                                    class="fa fa-trash"></span> Hapus</a>
                                            @endif

                                            @if(auth()->guard('web')->user()->role_id == 2 && $dataItem->status ==
                                            'MENUNGGU PERSETUJUAN')
                                            <a href="{{ route('sakit.edit', ['id'=>$dataItem->id]) }}"
                                                class="btn btn-warning"><span class="fa fa-edit"></span>
                                                Ubah</a>
                                            <a href="{{ route('sakit.delete', $dataItem->id) }}" class="btn btn-danger"
                                                onclick="return confirm('Apakah Anda Yakin Ingin Menghapus Ini ??');"><span
                                                    class="fa fa-trash"></span> Hapus</a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- [ Main Content ] start -->

@endsection
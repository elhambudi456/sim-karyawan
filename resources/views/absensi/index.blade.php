@extends('layouts')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Data Absensi</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Absensi</a></li>
                        <li class="breadcrumb-item active">Beranda</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-12 col-12">
                    <div class="card">
                        <div class="card-header bg-maroon">
                            <h3 class="card-title">Data Absensi</h3>
                            <div class="card-tools">
                                <a href="{{ route('absensi.create') }}" class="btn-tool"
                                    style="color:white !important;"><i class="fa fa-plus"></i> Tambah</a>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <strong>{{ $message }}</strong>
                            </div>
                            @endif
                            @if ($message = Session::get('danger'))
                            <div class="alert alert-danger">
                                <strong>{{ $message }}</strong>
                            </div>
                            @endif
                            <table id="example1" class="table table-striped table-bordered nowrap">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>User</th>
                                        <th>Tanggal</th>
                                        <th>Jam Masuk</th>
                                        <th>Jam Keluar</th>
                                        <th>Keterangan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $key => $dataItem)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{ $dataItem->name }}</td>
                                        <td>{{ $dataItem->tanggal }}</td>
                                        <td>{{ $dataItem->jam_masuk }}</td>
                                        <td>{{ $dataItem->jam_keluar }}</td>
                                        <td>{{ $dataItem->keterangan }}</td>
                                        <td>
                                            <?php if ($role == 1) { ?>
                                            <a href="{{ route('absensi.edit', ['id'=>$dataItem->id]) }}"
                                                class="btn btn-warning"><span class="fa fa-edit"></span> Ubah</a>
                                            <a href="{{ route('absensi.delete', $dataItem->id) }}"
                                                class="btn btn-danger"
                                                onclick="return confirm('Apakah Anda Yakin Ingin Menghapus Ini ??');"><span
                                                    class="fa fa-trash"></span> Hapus</button>
                                                <?php } else if ($role == 2) { ?>
                                                <a href="{{ route('absensi.checkout', ['id'=>$dataItem->id]) }}"
                                                    class="btn btn-warning"> Check out</a>
                                                <?php } ?>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- [ Main Content ] start -->

@endsection
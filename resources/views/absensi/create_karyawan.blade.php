@extends('layouts')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Data Absensi</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Absensi</a></li>
                        <li class="breadcrumb-item active">Pengajuan Absensi</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-12 col-12">
                    <div class="card">
                        <div class="card-header bg-primary">
                            <h3 class="card-title">Pengajuan Absensi</h3>
                        </div>
                        <!-- /.card-header -->
                        <form class="form-horizontal" method="post" action="{{ route('absensi.submit') }}">
                            @csrf
                            <div class="card-body">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    Mohon Isi Field Dengan Benar
                                    {{ $errors }}
                                </div>
                                @endif
                                <input type="hidden" name="role" value="{{$role}}">
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Keterangan</label>
                                    <textarea class="form-control" name="keterangan" id="" cols="30" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-save" style="color:white;"></i> Check in</button>
                                <a href="{{ route('jabatan.dashboard') }}" class="btn btn-secondary"><i class="fa fa-arrow-left" style="color:white;"></i> Kembali</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
@extends('layouts')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Data Profil</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Profil</a></li>
                        <li class="breadcrumb-item active">Ubah Data</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-12 col-12">
                    <div class="card">
                        <div class="card-header bg-warning">
                            <h3 class="card-title">Ubah Data Profil</h3>
                        </div>
                        <!-- /.card-header -->
                        <form class="form-horizontal" method="post"
                            action="{{ route('profile.update',['id'=>$data->id]) }}" enctype='multipart/form-data'>
                            @csrf
                            <div class="card-body">
                                @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <strong>{{ $message }}</strong>
                                </div>
                                @endif
                                @if ($message = Session::get('danger'))
                                <div class="alert alert-danger">
                                    <strong>{{ $message }}</strong>
                                </div>
                                @endif
                                <input class="form-control" value="{{ $dataProfil->id }}" type="hidden" name="idProfil">
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Nama</label>
                                    <input class="form-control" value="{{ $data->name }}" type="text" name="name"
                                        placeholder="Isikan Nama" required>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Username</label>
                                    <input class="form-control" type="text" value="{{ $data->username }}"
                                        name="username" placeholder="Isikan Username" required>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Password</label>
                                    <input class="form-control" type="password" name="password"
                                        placeholder="Isikan Password">
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Jabatan</label>
                                    <select class="form-control" name="jabatan_id" required>
                                        <option value=""> -- Pilih Jabatan Karyawan -- </option>
                                        @foreach ($jabatan as $key => $jabatanItem)
                                        @if($dataProfil->jabatan_id == $jabatanItem->id)
                                        <option value="{{ $jabatanItem->id }}" selected>
                                            {{$jabatanItem->nama}}
                                        </option>
                                        @else
                                        <option value="{{ $jabatanItem->id }}">
                                            {{$jabatanItem->nama}}
                                        </option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Divisi</label>
                                    <select class="form-control" name="divisi_id" required>
                                        <option value=""> -- Pilih Divisi Karyawan -- </option>
                                        @foreach ($divisi as $key => $divisiItem)
                                        @if($dataProfil->divisi_id == $divisiItem->id)
                                        <option value="{{ $divisiItem->id }}" selected>
                                            {{$divisiItem->nama}}
                                        </option>
                                        @else
                                        <option value="{{ $divisiItem->id }}">
                                            {{$divisiItem->nama}}
                                        </option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label pt-0">No Telp</label>
                                    <input class="form-control" value="{{ $dataProfil->phone }}" type="text"
                                        name="phone" placeholder="Isikan No Telepon" required>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Image</label>
                                    <br>
                                    <img src="{{ asset('karyawan/'.$image) }}" width="100px" height="100px">
                                    <br>
                                    <input class="form-control" type="file" name="image" accept="image/*">
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-warning" type="submit"><i class="fa fa-edit"></i> Ubah</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
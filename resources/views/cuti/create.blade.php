@extends('layouts')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Data Cuti</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Cuti</a></li>
                        <li class="breadcrumb-item active">Tambah Data</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-12 col-12">
                    <div class="card">
                        <div class="card-header bg-primary">
                            <h3 class="card-title">Tambah Data Cuti</h3>
                        </div>
                        <!-- /.card-header -->
                        <form class="form-horizontal" method="post" action="{{ route('cuti.submit') }}"
                            enctype='multipart/form-data'>
                            @csrf
                            <div class="card-body">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    Mohon Isi Field Dengan Benar
                                    {{ $errors }}
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Jenis Cuti</label>
                                    <select class="form-control" name="jenis_cuti_id" required>
                                        <option value=""> -- Pilih Jenis Cuti -- </option>
                                        @foreach ($jenisCuti as $key => $jenisCutiItem)
                                        <option value="{{ $jenisCutiItem->id }}">
                                            {{$jenisCutiItem->nama}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>

                                @if(auth()->guard('web')->user()->role_id == 1)
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Karyawan</label>
                                    <select class="form-control" name="user_id" required>
                                        <option value=""> -- Pilih Karyawan -- </option>
                                        @foreach ($karyawan as $key => $karyawanItem)
                                        <option value="{{ $karyawanItem->id }}">
                                            {{$karyawanItem->name}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                @else
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Karyawan</label>
                                    <input type="hidden" value="{{ auth()->guard('web')->user()->id }}" name="user_id">
                                    <input class="form-control" type="text"
                                        value="{{ auth()->guard('web')->user()->name }}"
                                        placeholder="Isikan Nama Karyawan" readonly>
                                </div>
                                @endif

                                <div class="form-group">
                                    <label class="col-form-label pt-0">Tanggal Awal</label>
                                    <input class="form-control" type="date" name="tanggal_awal"
                                        placeholder="Isikan Tanggal Awal Cuti" required>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Tanggal Akhir</label>
                                    <input class="form-control" type="date" name="tanggal_akhir"
                                        placeholder="Isikan Tanggal Akhir Cuti" required>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Keterangan</label>
                                    <textarea class="form-control" type="text" name="keterangan"
                                        placeholder="Isikan Keterangan"></textarea>
                                </div>
                                @if(auth()->guard('web')->user()->role_id == 1)
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Status</label>
                                    <select class="form-control" name="status" required>
                                        <option value=""> -- Pilih Status Pengajuan -- </option>
                                        @foreach ($status as $key => $statusItem)
                                        <option value="{{ $key }}">
                                            {{$statusItem}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                @endif
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-save"
                                        style="color:white;"></i> Simpan</button>
                                <a href="{{ route('cuti.dashboard') }}" class="btn btn-secondary"><i
                                        class="fa fa-arrow-left" style="color:white;"></i> Kembali</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
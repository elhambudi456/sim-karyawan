@extends('layouts-home')

@section('content')
<section class="home-section home-parallax home-fade home-full-height bg-dark-60 agency-page-header" id="home"
    data-background="{{ asset('landing/img/bg.jpg') }}">
    <div class="titan-caption">
        <div class="caption-content">
            <div class="font-alt mb-30 titan-title-size-1">Sistem Informasi Manajemen Karyawan</div>
            <div class="font-alt mb-40 titan-title-size-3">Membuat Karyawan <br>Menjadi Lebih Mudah</div>
            <a class="section-scroll btn btn-border-w btn-circle" href="{{ route('home.login') }}">Login
                Disini</a>
        </div>
    </div>
</section>
<div class="main">
    <section class="module">
        <div class="container">
            <div class="row multi-columns-row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="features-item">
                        <div class="features-icon"><span class="icon-lightbulb"></span></div>
                        <h3 class="features-title font-alt">Absensi</h3>Mempermudah karyawan untuk mengajukan
                        absensi melalui online
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="features-item">
                        <div class="features-icon"><span class="icon-tools"></span></div>
                        <h3 class="features-title font-alt">Pengajuan Cuti</h3>Mempermudah karyawan untuk
                        mengajukan
                        cuti melalui online
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="features-item">
                        <div class="features-icon"><span class="icon-tools-2"></span></div>
                        <h3 class="features-title font-alt">Pengajuan Izin</h3>Mempermudah karyawan untuk
                        mengajukan
                        izin melalui online
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="features-item">
                        <div class="features-icon"><span class="icon-lifesaver"></span></div>
                        <h3 class="features-title font-alt">Pengajuan Sakit</h3>Mempermudah karyawan untuk
                        mengajukan
                        sakit melalui online
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer class="footer bg-dark">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <p class="copyright font-alt">&copy; 2017&nbsp;<a href="#">SIM Karyawan</a>, All
                        Rights
                        Reserved</p>
                </div>
            </div>
        </div>
    </footer>
</div>
@endsection
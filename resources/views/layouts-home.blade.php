<!DOCTYPE html>
<html lang="en-US" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--  
    Document Title
    =============================================
    -->
    <title>Sistem Informasi Karyawan</title>
    <meta name="theme-color" content="#ffffff">
    <link rel="shortcut icon" href="{{ asset('logo/logo.png') }}">
    <!--  
    Stylesheets
    =============================================
    
    -->
    <!-- Default stylesheets-->
    <link href="{{ asset('landing/lib/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Template specific stylesheets-->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Volkhov:400i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="{{ asset('landing/lib/animate.css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('landing/lib/components-font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('landing/lib/et-line-font/et-line-font.css') }}" rel="stylesheet">
    <link href="{{ asset('landing/lib/flexslider/flexslider.css') }}" rel="stylesheet">
    <link href="{{ asset('landing/lib/owl.carousel/dist/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('landing/lib/owl.carousel/dist/assets/owl.theme.default.min.css') }}" rel="stylesheet">
    <link href="{{ asset('landing/lib/magnific-popup/dist/magnific-popup.css') }}" rel="stylesheet">
    <link href="{{ asset('landing/lib/simple-text-rotator/simpletextrotator.css') }}" rel="stylesheet">
    <!-- Main stylesheet and color file-->
    <link href="{{ asset('landing/css/style.css') }}" rel="stylesheet">
    <link id="color-scheme" href="{{ asset('landing/css/colors/default.css') }}" rel="stylesheet">
</head>

<body data-spy="scroll" data-target=".onpage-navigation" data-offset="60">
    <main>
        <div class="page-loader">
            <div class="loader">Loading...</div>
        </div>
        <nav class="navbar navbar-custom navbar-fixed-top navbar-transparent" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggle" type="button" data-toggle="collapse"
                        data-target="#custom-collapse"><span class="sr-only">Toggle navigation</span><span
                            class="icon-bar"></span><span class="icon-bar"></span><span
                            class="icon-bar"></span></button><a class="navbar-brand"
                        href="{{ route('home.index') }}">SIM Karyawan</a>
                </div>
                <div class="collapse navbar-collapse" id="custom-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="{{ route('home.index') }}">Beranda</a></li>
                        <li><a href="#totop">Pengumuman</a></li>
                        <li><a href="{{ route('home.login') }}">Login</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        @yield('content')
        <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a></div>
    </main>
    <!--  
    JavaScripts
    =============================================
    -->
    <script src="{{ asset('landing/lib/jquery/dist/jquery.js') }}"></script>
    <script src="{{ asset('landing/lib/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('landing/lib/wow/dist/wow.js') }}"></script>
    <script src="{{ asset('landing/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js') }}"></script>
    <script src="{{ asset('landing/lib/isotope/dist/isotope.pkgd.js') }}"></script>
    <script src="{{ asset('landing/lib/imagesloaded/imagesloaded.pkgd.js') }}"></script>
    <script src="{{ asset('landing/lib/flexslider/jquery.flexslider.js') }}"></script>
    <script src="{{ asset('landing/lib/owl.carousel/dist/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('landing/lib/smoothscroll.js') }}"></script>
    <script src="{{ asset('landing/lib/magnific-popup/dist/jquery.magnific-popup.js') }}"></script>
    <script src="{{ asset('landing/lib/simple-text-rotator/jquery.simple-text-rotator.min.js') }}"></script>
    <script src="{{ asset('landing/js/plugins.js') }}"></script>
    <script src="{{ asset('landing/js/main.js') }}"></script>
</body>

</html>
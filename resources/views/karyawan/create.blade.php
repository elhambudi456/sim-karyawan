@extends('layouts')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Data Karyawan</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Karyawan</a></li>
                        <li class="breadcrumb-item active">Tambah Data</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-12 col-12">
                    <div class="card">
                        <div class="card-header bg-primary">
                            <h3 class="card-title">Tambah Data Karyawan</h3>
                        </div>
                        <!-- /.card-header -->
                        <form class="form-horizontal" method="post" action="{{ route('karyawan.submit') }}"
                            enctype='multipart/form-data'>
                            @csrf
                            <div class="card-body">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    Mohon Isi Field Dengan Benar
                                    {{ $errors }}
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Nama</label>
                                    <input class="form-control" type="text" name="name" placeholder="Isikan Nama"
                                        required>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Username</label>
                                    <input class="form-control" type="text" name="username"
                                        placeholder="Isikan Username" required>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Password</label>
                                    <input class="form-control" type="password" name="password"
                                        placeholder="Isikan Password" required>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Jabatan</label>
                                    <select class="form-control" name="jabatan_id" required>
                                        <option value=""> -- Pilih Jabatan Karyawan -- </option>
                                        @foreach ($jabatan as $key => $jabatanItem)
                                        <option value="{{ $jabatanItem->id }}">
                                            {{$jabatanItem->nama}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Divisi</label>
                                    <select class="form-control" name="divisi_id" required>
                                        <option value=""> -- Pilih Divisi Karyawan -- </option>
                                        @foreach ($divisi as $key => $divisiItem)
                                        <option value="{{ $divisiItem->id }}">
                                            {{$divisiItem->nama}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label pt-0">No Telp</label>
                                    <input class="form-control" type="text" name="phone" placeholder="Isikan No Telepon"
                                        required>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Image</label>
                                    <input class="form-control" type="file" name="image" accept="image/*">
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-save"
                                        style="color:white;"></i> Simpan</button>
                                <a href="{{ route('karyawan.dashboard') }}" class="btn btn-secondary"><i
                                        class="fa fa-arrow-left" style="color:white;"></i> Kembali</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
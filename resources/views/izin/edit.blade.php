@extends('layouts')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Data Izin</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Izin</a></li>
                        <li class="breadcrumb-item active">Ubah Data</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-12 col-12">
                    <div class="card">
                        <div class="card-header bg-warning">
                            <h3 class="card-title">Ubah Data Izin</h3>
                        </div>
                        <!-- /.card-header -->
                        <form class="form-horizontal" method="post"
                            action="{{ route('izin.update',['id'=>$data->id]) }}" enctype='multipart/form-data'>
                            @csrf
                            <div class="card-body">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    Mohon Isi Field Dengan Benar
                                    {{ $errors }}
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Jenis Izin</label>
                                    <select class="form-control" name="jenis_izin_id" required>
                                        <option value=""> -- Pilih Jenis Izin -- </option>
                                        @foreach ($jenisIzin as $key => $jenisIzinItem)
                                        @if($data->jenis_izin_id == $jenisIzinItem->id)
                                        <option value="{{ $jenisIzinItem->id }}" selected>
                                            {{$jenisIzinItem->nama}}
                                        </option>
                                        @else
                                        <option value="{{ $jenisIzinItem->id }}">
                                            {{$jenisIzinItem->nama}}
                                        </option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                                @if(auth()->guard('web')->user()->role_id == 1)
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Karyawan</label>
                                    <select class="form-control" name="user_id" required>
                                        <option value=""> -- Pilih Karyawan -- </option>
                                        @foreach ($karyawan as $key => $karyawanItem)
                                        @if($data->user_id == $karyawanItem->id)
                                        <option value="{{ $karyawanItem->id }}" selected>
                                            {{$karyawanItem->name}}
                                        </option>
                                        @else
                                        <option value="{{ $karyawanItem->id }}">
                                            {{$karyawanItem->name}}
                                        </option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                                @else
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Karyawan</label>
                                    <input type="hidden" value="{{ auth()->guard('web')->user()->id }}" name="user_id">
                                    <input class="form-control" type="text"
                                        value="{{ auth()->guard('web')->user()->name }}"
                                        placeholder="Isikan Nama Karyawan" readonly>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Tanggal</label>
                                    <input class="form-control" type="date" value="{{ $data->tanggal }}" name="tanggal"
                                        placeholder="Isikan Tanggal Izin" required>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Jam Awal</label>
                                    <input class="form-control" type="time" value="{{ $data->jam_awal }}"
                                        name="jam_awal" placeholder="Isikan Jam Awal Izin" required>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Jam Akhir</label>
                                    <input class="form-control" type="time" value="{{ $data->jam_akhir }}"
                                        name="jam_akhir" placeholder="Isikan Jam Akhir Izin" required>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Keterangan</label>
                                    <textarea class="form-control" type="text" name="keterangan"
                                        placeholder="Isikan Keterangan">{{ $data->keterangan }}</textarea>
                                </div>
                                @if(auth()->guard('web')->user()->role_id == 1)
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Status</label>
                                    <select class="form-control" name="status" required>
                                        <option value=""> -- Pilih Status Pengajuan -- </option>
                                        @foreach ($status as $key => $statusItem)
                                        @if($data->status == $key)
                                        <option value="{{ $key }}" selected>
                                            {{$statusItem}}
                                        </option>
                                        @else
                                        <option value="{{ $key }}">
                                            {{$statusItem}}
                                        </option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                                @endif
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-warning" type="submit"><i class="fa fa-edit"></i> Ubah</button>
                                <a href="{{ route('izin.dashboard') }}" class="btn btn-secondary"><i
                                        class="fa fa-arrow-left" style="color:white;"></i> Kembali</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
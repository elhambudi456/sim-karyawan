@extends('layouts')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Data Divisi</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Divisi</a></li>
                        <li class="breadcrumb-item active">Ubah Data</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-12 col-12">
                    <div class="card">
                        <div class="card-header bg-warning">
                            <h3 class="card-title">Ubah Data Divisi</h3>
                        </div>
                        <!-- /.card-header -->
                        <form class="form-horizontal" method="post"
                            action="{{ route('divisi.update',['id'=>$data->id]) }}">
                            @csrf
                            <div class="card-body">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    Mohon Isi Field Dengan Benar
                                    {{ $errors }}
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Kode</label>
                                    <input class="form-control" value="{{ $data->kode }}" type="text" name="kode"
                                        placeholder="Isikan Kode" required>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Nama</label>
                                    <input class="form-control" value="{{ $data->nama }}" type="text" name="nama"
                                        placeholder="Isikan Nama" required>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-warning" type="submit"><i class="fa fa-edit"></i> Ubah</button>
                                <a href="{{ route('divisi.dashboard') }}" class="btn btn-secondary"><i
                                        class="fa fa-arrow-left" style="color:white;"></i> Kembali</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
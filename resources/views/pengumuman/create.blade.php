@extends('layouts')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Data Pengumuman</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Pengumuman</a></li>
                        <li class="breadcrumb-item active">Tambah Data</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-12 col-12">
                    <div class="card">
                        <div class="card-header bg-primary">
                            <h3 class="card-title">Tambah Data Pengumuman</h3>
                        </div>
                        <!-- /.card-header -->
                        <form class="form-horizontal" method="post" action="{{ route('pengumuman.submit') }}">
                            @csrf
                            <div class="card-body">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    Mohon Isi Field Dengan Benar
                                    {{ $errors }}
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Judul</label>
                                    <input class="form-control" type="text" name="judul" placeholder="Isikan Judul"
                                        required>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Deskripsi Singkat</label>
                                    <textarea class="form-control" name="deskripsi_singkat"
                                        placeholder="Isikan Deskripsi Singkat" required></textarea>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Konten</label>
                                    <textarea class="form-control myeditor" name="konten"
                                        placeholder="Isikan Konten"></textarea>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Status</label>
                                    <select class="form-control" name="status" required>
                                        <option value=""> -- Pilih Status -- </option>
                                        @foreach ($status as $key => $statusItem)
                                        <option value="{{ $key }}">
                                            {{$statusItem}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-save"
                                        style="color:white;"></i> Simpan</button>
                                <a href="{{ route('pengumuman.dashboard') }}" class="btn btn-secondary"><i
                                        class="fa fa-arrow-left" style="color:white;"></i> Kembali</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
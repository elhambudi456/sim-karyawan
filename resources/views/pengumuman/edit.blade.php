@extends('layouts')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Data Pengumuman</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Pengumuman</a></li>
                        <li class="breadcrumb-item active">Ubah Data</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-12 col-12">
                    <div class="card">
                        <div class="card-header bg-warning">
                            <h3 class="card-title">Ubah Data Pengumuman</h3>
                        </div>
                        <!-- /.card-header -->
                        <form class="form-horizontal" method="post"
                            action="{{ route('pengumuman.update',['id'=>$data->id]) }}">
                            @csrf
                            <div class="card-body">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    Mohon Isi Field Dengan Benar
                                    {{ $errors }}
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Judul</label>
                                    <input class="form-control" type="text" value="{{ $data->judul }}" name="judul"
                                        placeholder="Isikan Judul" required>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Deskripsi Singkat</label>
                                    <textarea class="form-control" name="deskripsi_singkat"
                                        placeholder="Isikan Deskripsi Singkat"
                                        required>{{ $data->deskripsi_singkat }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Konten</label>
                                    <textarea class="form-control myeditor" name="konten"
                                        placeholder="Isikan Konten">{!! $data->konten !!}</textarea>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label pt-0">Status</label>
                                    <select class="form-control" name="status" required>
                                        <option value=""> -- Pilih Status -- </option>
                                        @foreach ($status as $key => $statusItem)
                                        @if($data->status == $key)
                                        <option value="{{ $key }}" selected>
                                            {{$statusItem}}
                                        </option>
                                        @else
                                        <option value="{{ $key }}">
                                            {{$statusItem}}
                                        </option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-warning" type="submit"><i class="fa fa-edit"></i> Ubah</button>
                                <a href="{{ route('pengumuman.dashboard') }}" class="btn btn-secondary"><i
                                        class="fa fa-arrow-left" style="color:white;"></i> Kembali</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
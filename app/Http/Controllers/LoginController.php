<?php

namespace App\Http\Controllers;

use Auth,Validator;
use App\Models\AboutUs;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/dashboard';

    protected $username;

    public function __construct()
    {
      $this->middleware('guest:web')->except('logout');
      $this->username = 'username';
    }

    public function guard()
    {
     return Auth::guard('web');
    }

    public function showLoginForm()
    {
        $data = array();
        return view('login.index',compact(
            'data'        
        ));
    }

    public function login(Request $request)
    {
        //VALIDASI DATA YANG DITERIMA
        $this->validator((array)$request);

        //CUKUP MENGAMBIL EMAIL DAN PASSWORD SAJA DARI REQUEST
        //KARENA JUGA DISERTAKAN TOKEN
        $auth = $request->only('username', 'password');
        //$auth['status'] = 1; //TAMBAHKAN JUGA STATUS YANG BISA LOGIN HARUS 1
      
        //CHECK UNTUK PROSES OTENTIKASI
        //DARI GUARD CUSTOMER, KITA ATTEMPT PROSESNYA DARI DATA $AUTH
        if (auth()->guard('web')->attempt($auth)) {
            //JIKA BERHASIL MAKA AKAN DIREDIRECT KE DASHBOARD
            return redirect()->intended(route('home.dashboard'));
        }
        //JIKA GAGAL MAKA REDIRECT KEMBALI BERSERTA NOTIFIKASI
        return redirect()->back()->withErrors([
            'username' => 'Username Salah',
            'password' => 'Password Salah',
        ]);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => ['required'],
            'password' => ['required'],
        ]);
    }

    /**
     * Get username property.
     *
     * @return string
     */
    public function username()
    {
        return $this->username;
    }

    public function logout()
    {
        Auth::guard('web')->logout();
        return redirect()->to('/login')->with('success','Berhasil Logout dari Akun Anda.');
    }
}
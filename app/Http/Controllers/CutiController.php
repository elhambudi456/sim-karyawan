<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\PengajuanCuti;
use App\Models\User;
use App\Models\JenisCuti;
use Auth,File;


class CutiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user');
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function index(Request $request)
    {           
        $data = PengajuanCuti::query()->orderBy('created_at', 'asc')->get(); 
        return view('cuti.index', compact(
            'data'
        ));
    }

    public function create()
    {           
        $karyawan = User::query()->where('id','<>',1)->get(); 
        $jenisCuti = JenisCuti::query()->get();
        $status = array(
            'MENUNGGU PERSETUJUAN' => 'MENUNGGU PERSETUJUAN',
            'DITERIMA' => 'DITERIMA',
            'DITOLAK' => 'DITOLAK'
        ); 
        return view('cuti.create',compact(
            'karyawan',
            'jenisCuti',
            'status'
        ));
    }

    public function submit(Request $request)
    {         
        $data = new PengajuanCuti;

        $data->user_id    = $request->get('user_id');
        $data->jenis_cuti_id    = $request->get('jenis_cuti_id');
        $data->tanggal_awal    = $request->get('tanggal_awal');
        $data->tanggal_akhir    = $request->get('tanggal_akhir');
        $data->keterangan    = $request->get('keterangan');
        if(Auth::guard('web')->user()->role_id == 1){
            $data->status    = $request->get('status');
        }else{
            $data->status    = 'MENUNGGU PERSETUJUAN';
        }
        $data->created_at = date("Y-m-d H:i:s");
        $data->updated_at = date("Y-m-d H:i:s");
            
        if($data->save()){
            return redirect()->route('cuti.dashboard')->with('success','Berhasil Disimpan.');
        }else{
            return redirect()->route('cuti.dashboard')->with('danger','Gagal Disimpan.');
        }
    }

    public function edit(Request $request, $id)
    {           
        $data = PengajuanCuti::findOrFail($id);
        $karyawan = User::query()->where('id','<>',1)->get(); 
        $jenisCuti = JenisCuti::query()->get();
        $status = array(
            'MENUNGGU PERSETUJUAN' => 'MENUNGGU PERSETUJUAN',
            'DITERIMA' => 'DITERIMA',
            'DITOLAK' => 'DITOLAK'
        ); 
        return view('cuti.edit',compact(
            'data',
            'karyawan',
            'jenisCuti',
            'status'
        ));
    }

    public function update($id , Request $request)
    {         
        $data = PengajuanCuti::findOrFail($id);
        $data->user_id    = $request->get('user_id');
        $data->jenis_cuti_id    = $request->get('jenis_cuti_id');
        $data->tanggal_awal    = $request->get('tanggal_awal');
        $data->tanggal_akhir    = $request->get('tanggal_akhir');
        $data->keterangan    = $request->get('keterangan');
        if(Auth::guard('web')->user()->role_id == 1){
            $data->status    = $request->get('status');
        }else{
            $data->status    = 'MENUNGGU PERSETUJUAN';
        }
        $data->updated_at = date("Y-m-d H:i:s");
            
        if($data->save()){
            return redirect()->route('cuti.dashboard')->with('success','Berhasil Disimpan.');
        }else{
            return redirect()->route('cuti.dashboard')->with('danger','Gagal Disimpan.');
        }
    }

    public function delete($id)
    {   
        $data = PengajuanCuti::findOrFail($id);    
        $data->delete();
        return redirect()->route('cuti.dashboard')->with('success','Berhasil Dihapus.');    
    }

    public function terima($id)
    {   
        $data = PengajuanCuti::findOrFail($id);   
        $data->status = 'DITERIMA'; 
        $data->save();
        return redirect()->route('cuti.dashboard')->with('success','Berhasil Diterima.');    
    }

    public function tolak($id)
    {   
        $data = PengajuanCuti::findOrFail($id);    
        $data->status = 'DITOLAK'; 
        $data->save();
        return redirect()->route('cuti.dashboard')->with('success','Berhasil Ditolak.');    
    }
}
<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\PengajuanSakit;
use App\Models\User;
use App\Models\JenisSakit;
use Auth,File;


class SakitController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user');
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function index(Request $request)
    {           
        $data = PengajuanSakit::query()->orderBy('created_at', 'asc')->get(); 
        return view('sakit.index', compact(
            'data'
        ));
    }

    public function create()
    {           
        $karyawan = User::query()->where('id','<>',1)->get();
        $status = array(
            'MENUNGGU PERSETUJUAN' => 'MENUNGGU PERSETUJUAN',
            'DITERIMA' => 'DITERIMA',
            'DITOLAK' => 'DITOLAK'
        ); 
        return view('Sakit.create',compact(
            'karyawan',
            'status'
        ));
    }

    public function submit(Request $request)
    {         
        $destpath = public_path().'/pengajuan-sakit/';
        $data = new PengajuanSakit;
        if ($request->hasFile('file'))
        {
            $rand = md5(date('dmYHism').mt_rand(11111, 99999).$request->file->getClientOriginalName());
            $pic = $request->file->move($destpath, $rand.'.'.$request->file->getClientOriginalExtension());
            $data->file = $rand.'.'.$request->file->getClientOriginalExtension();
        }
        $data->user_id    = $request->get('user_id');
        $data->tanggal_awal    = $request->get('tanggal_awal');
        $data->tanggal_akhir    = $request->get('tanggal_akhir');
        $data->keterangan    = $request->get('keterangan');
        if(Auth::guard('web')->user()->role_id == 1){
            $data->status    = $request->get('status');
        }else{
            $data->status    = 'MENUNGGU PERSETUJUAN';
        }
        $data->created_at = date("Y-m-d H:i:s");
        $data->updated_at = date("Y-m-d H:i:s");
            
        if($data->save()){
            return redirect()->route('sakit.dashboard')->with('success','Berhasil Disimpan.');
        }else{
            return redirect()->route('sakit.dashboard')->with('danger','Gagal Disimpan.');
        }
    }

    public function edit(Request $request, $id)
    {           
        $data = PengajuanSakit::findOrFail($id);
        $karyawan = User::query()->where('id','<>',1)->get(); 
        $status = array(
            'MENUNGGU PERSETUJUAN' => 'MENUNGGU PERSETUJUAN',
            'DITERIMA' => 'DITERIMA',
            'DITOLAK' => 'DITOLAK'
        ); 
        return view('sakit.edit',compact(
            'data',
            'karyawan',
            'status'
        ));
    }

    public function update($id , Request $request)
    {         
        $destpath = public_path().'/pengajuan-sakit/';
        $data = PengajuanSakit::findOrFail($id);
        if ($request->hasFile('file'))
        {
            $rand = md5(date('dmYHism').mt_rand(11111, 99999).$request->file->getClientOriginalName());
            $pic = $request->file->move($destpath, $rand.'.'.$request->file->getClientOriginalExtension());
            $data->file = $rand.'.'.$request->file->getClientOriginalExtension();
        }
        $data->user_id    = $request->get('user_id');
        $data->tanggal_awal    = $request->get('tanggal_awal');
        $data->tanggal_akhir    = $request->get('tanggal_akhir');
        $data->keterangan    = $request->get('keterangan');
        if(Auth::guard('web')->user()->role_id == 1){
            $data->status    = $request->get('status');
        }else{
            $data->status    = 'MENUNGGU PERSETUJUAN';
        }
        $data->updated_at = date("Y-m-d H:i:s");
            
        if($data->save()){
            return redirect()->route('sakit.dashboard')->with('success','Berhasil Disimpan.');
        }else{
            return redirect()->route('sakit.dashboard')->with('danger','Gagal Disimpan.');
        }
    }

    public function delete($id)
    {   
        $data = PengajuanSakit::findOrFail($id);    
        $data->delete();
        return redirect()->route('sakit.dashboard')->with('success','Berhasil Dihapus.');    
    }

    public function terima($id)
    {   
        $data = PengajuanSakit::findOrFail($id);   
        $data->status = 'DITERIMA'; 
        $data->save();
        return redirect()->route('sakit.dashboard')->with('success','Berhasil Diterima.');    
    }

    public function tolak($id)
    {   
        $data = PengajuanSakit::findOrFail($id);    
        $data->status = 'DITOLAK'; 
        $data->save();
        return redirect()->route('sakit.dashboard')->with('success','Berhasil Ditolak.');    
    }
}
<?php

namespace App\Http\Controllers;

use App\Models\PengajuanAbsensi;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Ui\Presets\React;

class AbsensiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('user');
    }

    public function index(Request $request)
    {
        if (Auth::user()->role_id == 1) {
            $data['data'] = PengajuanAbsensi::query()->join('users', 'pengajuan_absensi.user_id', '=', 'users.id')->orderBy('pengajuan_absensi.created_at', 'asc')->get();
        } else if (Auth::user()->role_id == 2) {
            $data['data'] = PengajuanAbsensi::query()->orderBy('created_at', 'asc')->where('user_id', Auth::user()->id)->get();
        }
        $data['role'] = Auth::user()->role_id;
        return view('absensi.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->role_id == 1) {
            $data['data'] = User::query()->where('role_id', '2')->get();
            $data['role'] = Auth::user()->role_id;
            return view('absensi.create', $data);
        } else if (Auth::user()->role_id == 2) {
            $data['role'] = Auth::user()->role_id;
            return view('absensi.create_karyawan', $data);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function submit(Request $request)
    {
        $data = new PengajuanAbsensi;
        if (Auth::user()->role_id == 1) {
            $data->user_id    = $request->get('user');
            $data->tanggal    = $request->get('tanggal');
            $data->jam_masuk    = $request->get('jam_masuk');
            $data->jam_keluar    = $request->get('jam_keluar');
            $data->keterangan    = $request->get('keterangan');
            $data->status    = $request->get('status');
            $data->created_at = date("Y-m-d H:i:s");
            $data->updated_at = date("Y-m-d H:i:s");
        } else if (Auth::user()->role_id == 2) {
            $data->user_id    = Auth::user()->id;
            $data->tanggal    = date("Y-m-d");
            $data->jam_masuk    = date("H:i:s");
            $data->jam_keluar    = null;
            $data->keterangan    = $request->get('keterangan');
            $data->status    = 'Menunggu persetujuan';
            $data->created_at = date("Y-m-d H:i:s");
            $data->updated_at = date("Y-m-d H:i:s");
        }
        if ($data->save()) {
            return redirect()->route('absensi.dashboard')->with('success', 'Berhasil Disimpan.');
        } else {
            return redirect()->route('absensi.dashboard')->with('danger', 'Gagal Disimpan.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function checkout($id)
    {
        $data = PengajuanAbsensi::findOrFail($id);
        $data->jam_keluar    = date("H:i:s");
        if ($data->save()) {
            return redirect()->route('absensi.dashboard')->with('success', 'Berhasil Check Out.');
        } else {
            return redirect()->route('absensi.dashboard')->with('danger', 'Gagal Check Out.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['karyawan'] = User::query()->where('role_id', '2')->get();
        $data['edit'] = PengajuanAbsensi::find($id);
        return view('absensi.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = PengajuanAbsensi::findOrFail($id);
        $data->user_id    = $request->get('user');
        $data->tanggal    = $request->get('tanggal');
        $data->jam_masuk    = $request->get('jam_masuk');
        $data->jam_keluar    = $request->get('jam_keluar');
        $data->keterangan    = $request->get('keterangan');
        $data->status    = $request->get('status');
        $data->updated_at = date("Y-m-d H:i:s");

        if ($data->save()) {
            return redirect()->route('absensi.dashboard')->with('success', 'Berhasil Disimpan.');
        } else {
            return redirect()->route('absensi.dashboard')->with('danger', 'Gagal Disimpan.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data = PengajuanAbsensi::findOrFail($id);
        $data->delete();
        return redirect()->route('absensi.dashboard')->with('success', 'Berhasil Dihapus.');
    }
}
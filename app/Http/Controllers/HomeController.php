<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth,File;

class HomeController extends Controller
{
    public function index(Request $request){

    	$data = array();
        return view('home.index',compact(
            'data'
		));
    }
}
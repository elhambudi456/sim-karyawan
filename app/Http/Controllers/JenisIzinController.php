<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\JenisIzin;
use File;


class JenisIzinController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user');
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function index(Request $request)
    {           
        
        $data = JenisIzin::query()->orderBy('created_at', 'asc')->get(); 
        return view('jenis-izin.index', compact(
            'data'
        ));
    }

    public function create()
    {           
        $status = array('1'=>'Publish','0'=>'Draft');
        return view('jenis-izin.create',compact(
            'status'
        ));
    }

    public function submit(Request $request)
    {         
        $data = new JenisIzin;
        $data->nama    = $request->get('nama');
        $data->created_at = date("Y-m-d H:i:s");
        $data->updated_at = date("Y-m-d H:i:s");
            
        if($data->save()){
            return redirect()->route('jenisizin.dashboard')->with('success','Berhasil Disimpan.');
        }else{
            return redirect()->route('jenisizin.dashboard')->with('danger','Gagal Disimpan.');
        }
    }

    public function edit(Request $request, $id)
    {           
        $data = JenisIzin::findOrFail($id);
        return view('jenis-izin.edit',compact(
            'data'
        ));
    }

    public function update($id , Request $request)
    {         
        $data = JenisIzin::findOrFail($id);
        $data->nama    = $request->get('nama');
        $data->updated_at = date("Y-m-d H:i:s");
            
        if($data->save()){
            return redirect()->route('jenisizin.dashboard')->with('success','Berhasil Disimpan.');
        }else{
            return redirect()->route('jenisizin.dashboard')->with('danger','Gagal Disimpan.');
        }
    }

    public function delete($id)
    {   
        $data = JenisIzin::findOrFail($id);    
        $data->delete();
        return redirect()->route('jenisizin.dashboard')->with('success','Berhasil Dihapus.');    
    }
}
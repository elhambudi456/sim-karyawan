<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use File,Auth;
use App\Models\User;
use App\Models\JenisCuti;
use App\Models\JenisIzin;
use App\Models\Pengumuman;
use App\Models\PengajuanAbsensi;
use App\Models\PengajuanCuti;
use App\Models\PengajuanIzin;
use App\Models\PengajuanSakit;


class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user');
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function index(Request $request)
    {           
        $karyawan = User::query()->where('id','<>',1)->count();
        $jenisCuti = JenisCuti::query()->count();
        $jenisIzin = JenisIzin::query()->count();
        $pengumuman = Pengumuman::query()->count();
        $absensi = PengajuanAbsensi::query()->count();
        $cuti = PengajuanCuti::query()->count();
        $sakit = PengajuanSakit::query()->count();
        $izin = PengajuanIzin::query()->count();
        if(Auth::guard('web')->user()->role_id == 1){
            return view('home.dashboard', compact(
                'karyawan',
                'jenisCuti',
                'jenisIzin',
                'pengumuman',
                'absensi',
                'cuti',
                'sakit',
                'izin'
            ));
        }else{
            return view('home.karyawan', compact(
                'absensi',
                'cuti',
                'sakit',
                'izin'
            ));
        }
    }
}
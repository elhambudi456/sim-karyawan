<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Profil;
use App\Models\Jabatan;
use App\Models\Divisi;
use Auth,File;


class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user');
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function edit(Request $request)
    {           
        $id = Auth::guard('web')->user()->id;
        $data = User::findOrFail($id);
        $dataProfil = Profil::query()->where('user_id',$id)->first();
        $jabatan = Jabatan::query()->get();
        $divisi = Divisi::query()->get();
        $image = 'default.png';
        if(!empty($dataProfil->image)){
            $path = public_path().'/karyawan/'.$dataProfil->image;
            $isExists = File::exists($path);
            if($isExists){
                $image = $dataProfil->image;
            }
        }
        return view('profile.index',compact(
            'data',
            'dataProfil',
            'jabatan',
            'divisi',
            'image'
        ));
    }

    public function update($id , Request $request)
    {         
        $destpath = public_path().'/karyawan/';
        $data = User::findOrFail($id);
        $user = User::query()->where('username',$request->get('username'))->count();
        if($request->get('username') != $data->username){
            if($user > 0){
                return redirect()->route('profile.edit')->with('danger','Gagal Disimpan Dikarenakan Username Sudah Pernah Dipakai.');
            }
        }

        $data->name    = $request->get('name');
        $data->username    = $request->get('username');
        $data->email   = $request->get('username').'@email.com';
        $data->password    = bcrypt($request->get('password'));
        $data->created_at = date("Y-m-d H:i:s");
        $data->updated_at = date("Y-m-d H:i:s");
            
        if($data->save()){
            $dataProfile = Profil::findOrFail($request->get('idProfil'));
        
            if ($request->hasFile('image'))
            {
                $rand = md5(date('dmYHism').mt_rand(11111, 99999).$request->image->getClientOriginalName());
                $pic = $request->image->move($destpath, $rand.'.'.$request->image->getClientOriginalExtension());
                $dataProfile->image = $rand.'.'.$request->image->getClientOriginalExtension();
            }
            $dataProfile->user_id    = $data->id;
            $dataProfile->jabatan_id    = $request->get('jabatan_id');
            $dataProfile->divisi_id    = $request->get('divisi_id');
            $dataProfile->phone    = $request->get('phone');
            $dataProfile->save();
            return redirect()->route('profile.edit')->with('success','Berhasil Disimpan.');
        }else{
            return redirect()->route('profile.edit')->with('danger','Gagal Disimpan.');
        }
    }
}
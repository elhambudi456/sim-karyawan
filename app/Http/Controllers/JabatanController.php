<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Jabatan;
use File;


class JabatanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user');
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function index(Request $request)
    {           
        
        $data = Jabatan::query()->orderBy('created_at', 'asc')->get(); 
        return view('jabatan.index', compact(
            'data'
        ));
    }

    public function create()
    {           
        $status = array('1'=>'Publish','0'=>'Draft');
        return view('jabatan.create',compact(
            'status'
        ));
    }

    public function submit(Request $request)
    {         
        $data = new Jabatan;

        $data->kode    = $request->get('kode');
        $data->nama    = $request->get('nama');
        $data->created_at = date("Y-m-d H:i:s");
        $data->updated_at = date("Y-m-d H:i:s");
            
        if($data->save()){
            return redirect()->route('jabatan.dashboard')->with('success','Berhasil Disimpan.');
        }else{
            return redirect()->route('jabatan.dashboard')->with('danger','Gagal Disimpan.');
        }
    }

    public function edit(Request $request, $id)
    {           
        $data = Jabatan::findOrFail($id);
        return view('jabatan.edit',compact(
            'data'
        ));
    }

    public function update($id , Request $request)
    {         
        $data = Jabatan::findOrFail($id);
        $data->kode    = $request->get('kode');
        $data->nama    = $request->get('nama');
        $data->updated_at = date("Y-m-d H:i:s");
            
        if($data->save()){
            return redirect()->route('jabatan.dashboard')->with('success','Berhasil Disimpan.');
        }else{
            return redirect()->route('jabatan.dashboard')->with('danger','Gagal Disimpan.');
        }
    }

    public function delete($id)
    {   
        $data = Jabatan::findOrFail($id);    
        $data->delete();
        return redirect()->route('jabatan.dashboard')->with('success','Berhasil Dihapus.');    
    }
}
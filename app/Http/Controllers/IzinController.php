<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\PengajuanIzin;
use App\Models\User;
use App\Models\JenisIzin;
use Auth,File;


class IzinController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user');
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function index(Request $request)
    {           
        $data = PengajuanIzin::query()->orderBy('created_at', 'asc')->get(); 
        return view('izin.index', compact(
            'data'
        ));
    }

    public function create()
    {           
        $karyawan = User::query()->where('id','<>',1)->get(); 
        $jenisIzin = JenisIzin::query()->get();
        $status = array(
            'MENUNGGU PERSETUJUAN' => 'MENUNGGU PERSETUJUAN',
            'DITERIMA' => 'DITERIMA',
            'DITOLAK' => 'DITOLAK'
        ); 
        return view('izin.create',compact(
            'karyawan',
            'jenisIzin',
            'status'
        ));
    }

    public function submit(Request $request)
    {         
        $data = new PengajuanIzin;

        $data->user_id    = $request->get('user_id');
        $data->jenis_izin_id    = $request->get('jenis_izin_id');
        $data->tanggal    = $request->get('tanggal');
        $data->jam_awal    = $request->get('jam_awal');
        $data->jam_akhir    = $request->get('jam_akhir');
        $data->keterangan    = $request->get('keterangan');
        if(Auth::guard('web')->user()->role_id == 1){
            $data->status    = $request->get('status');
        }else{
            $data->status    = 'MENUNGGU PERSETUJUAN';
        }
        $data->created_at = date("Y-m-d H:i:s");
        $data->updated_at = date("Y-m-d H:i:s");
            
        if($data->save()){
            return redirect()->route('izin.dashboard')->with('success','Berhasil Disimpan.');
        }else{
            return redirect()->route('izin.dashboard')->with('danger','Gagal Disimpan.');
        }
    }

    public function edit(Request $request, $id)
    {           
        $data = PengajuanIzin::findOrFail($id);
        $karyawan = User::query()->where('id','<>',1)->get(); 
        $jenisIzin = JenisIzin::query()->get();
        $status = array(
            'MENUNGGU PERSETUJUAN' => 'MENUNGGU PERSETUJUAN',
            'DITERIMA' => 'DITERIMA',
            'DITOLAK' => 'DITOLAK'
        ); 
        return view('izin.edit',compact(
            'data',
            'karyawan',
            'jenisIzin',
            'status'
        ));
    }

    public function update($id , Request $request)
    {         
        $data = PengajuanIzin::findOrFail($id);
        $data->user_id    = $request->get('user_id');
        $data->jenis_izin_id    = $request->get('jenis_izin_id');
        $data->tanggal    = $request->get('tanggal');
        $data->jam_awal    = $request->get('jam_awal');
        $data->jam_akhir    = $request->get('jam_akhir');
        $data->keterangan    = $request->get('keterangan');
        if(Auth::guard('web')->user()->role_id == 1){
            $data->status    = $request->get('status');
        }else{
            $data->status    = 'MENUNGGU PERSETUJUAN';
        }
        $data->updated_at = date("Y-m-d H:i:s");
            
        if($data->save()){
            return redirect()->route('izin.dashboard')->with('success','Berhasil Disimpan.');
        }else{
            return redirect()->route('izin.dashboard')->with('danger','Gagal Disimpan.');
        }
    }

    public function delete($id)
    {   
        $data = PengajuanIzin::findOrFail($id);    
        $data->delete();
        return redirect()->route('izin.dashboard')->with('success','Berhasil Dihapus.');    
    }

    public function terima($id)
    {   
        $data = PengajuanIzin::findOrFail($id);   
        $data->status = 'DITERIMA'; 
        $data->save();
        return redirect()->route('izin.dashboard')->with('success','Berhasil Diterima.');    
    }

    public function tolak($id)
    {   
        $data = PengajuanIzin::findOrFail($id);    
        $data->status = 'DITOLAK'; 
        $data->save();
        return redirect()->route('izin.dashboard')->with('success','Berhasil Ditolak.');    
    }
}
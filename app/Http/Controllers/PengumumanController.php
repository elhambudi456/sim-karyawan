<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Pengumuman;
use File;


class PengumumanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user');
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function index(Request $request)
    {           
        
        $data = Pengumuman::query()->orderBy('created_at', 'asc')->get(); 
        return view('pengumuman.index', compact(
            'data'
        ));
    }

    public function create()
    {           
        $status = array(
            'PUBLISH' => 'PUBLISH',
            'DRAFT' => 'DRAFT'
        ); 
        return view('pengumuman.create',compact(
            'status'
        ));
    }

    public function submit(Request $request)
    {         
        $data = new Pengumuman;

        $data->judul    = $request->get('judul');
        $data->slug    = Str::slug($data->judul);
        $data->deskripsi_singkat    = $request->get('deskripsi_singkat');
        $data->konten    = $request->get('konten');
        $data->status    = $request->get('status');
        $data->created_at = date("Y-m-d H:i:s");
        $data->updated_at = date("Y-m-d H:i:s");
            
        if($data->save()){
            return redirect()->route('pengumuman.dashboard')->with('success','Berhasil Disimpan.');
        }else{
            return redirect()->route('pengumuman.dashboard')->with('danger','Gagal Disimpan.');
        }
    }

    public function edit(Request $request, $id)
    {           
        $data = Pengumuman::findOrFail($id);
        $status = array(
            'PUBLISH' => 'PUBLISH',
            'DRAFT' => 'DRAFT'
        ); 
        return view('pengumuman.edit',compact(
            'data',
            'status'
        ));
    }

    public function update($id , Request $request)
    {         
        $data = Pengumuman::findOrFail($id);
        $data->judul    = $request->get('judul');
        $data->slug    = Str::slug($data->judul);
        $data->deskripsi_singkat    = $request->get('deskripsi_singkat');
        $data->konten    = $request->get('konten');
        $data->status    = $request->get('status');
        $data->updated_at = date("Y-m-d H:i:s");
            
        if($data->save()){
            return redirect()->route('pengumuman.dashboard')->with('success','Berhasil Disimpan.');
        }else{
            return redirect()->route('pengumuman.dashboard')->with('danger','Gagal Disimpan.');
        }
    }

    public function delete($id)
    {   
        $data = Pengumuman::findOrFail($id);    
        $data->delete();
        return redirect()->route('pengumuman.dashboard')->with('success','Berhasil Dihapus.');    
    }
}
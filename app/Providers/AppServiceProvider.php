<?php

namespace App\Providers;

use Auth,File;
use App\Models\Role;
use App\Models\Profil;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) 
        {
            $imageProfile = 'default.png';
            $roleName = '';
            if (Auth::guard('web')->check()) {
                $dataProfil = Profil::query()->where('user_id',Auth::guard('web')->user()->id)->first();
                if(!empty($dataProfil->image)){
                    $path = public_path().'/karyawan/'.$dataProfil->image;
                    $isExists = File::exists($path);
                    if($isExists){
                        $imageProfile = $dataProfil->image;
                    }
                }
                $role = Role::findOrFail(Auth::guard('web')->user()->role_id);
                $roleName = $role->nama;
            }

            $view->with(compact(
                'imageProfile',
                'roleName'
            ));
        });  
    }
}
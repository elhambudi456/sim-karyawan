<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PengajuanSakit extends Model
{
    use HasFactory;

    protected $table = 'pengajuan_sakit';

    protected $fillable = [
        'user_id',
        'tanggal_awal',
        'tanggal_akhir',
        'file',
        'keterangan',
        'status'
    ];

    public function karyawan($id){
        $data = User::findOrFail($id);
        if(!empty($data)){
            return $data->name;
        }else{
            return '-';
        }
    }
}
<?php

namespace App\Models;

use App\Models\JenisIzin;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PengajuanIzin extends Model
{
    use HasFactory;

    protected $table = 'pengajuan_izin';

    protected $fillable = [
        'user_id',
        'jenis_izin_id',
        'tanggal',
        'jam_awal',
        'jam_akhir',
        'keterangan',
        'status'
    ];

    public function izin($id){
        $data = JenisIzin::findOrFail($id);
        if(!empty($data)){
            return $data->nama;
        }else{
            return '-';
        }
    }

    public function karyawan($id){
        $data = User::findOrFail($id);
        if(!empty($data)){
            return $data->name;
        }else{
            return '-';
        }
    }
}
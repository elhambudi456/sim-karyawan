<?php

namespace App\Models;

use App\Models\JenisCuti;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PengajuanCuti extends Model
{
    use HasFactory;

    protected $table = 'pengajuan_cuti';

    protected $fillable = [
        'user_id',
        'jenis_cuti_id',
        'tanggal_awal',
        'tanggal_akhir',
        'keterangan',
        'status'
    ];
    
    public function cuti($id){
        $data = JenisCuti::findOrFail($id);
        if(!empty($data)){
            return $data->nama;
        }else{
            return '-';
        }
    }

    public function karyawan($id){
        $data = User::findOrFail($id);
        if(!empty($data)){
            return $data->name;
        }else{
            return '-';
        }
    }
}